from frappe import _


class NextcloudException(Exception):
	pass


class NextcloudSyncMissingRoot(NextcloudException):
	def __init__(self, *args):
		super().__init__("no root directory", *args)


class NextcloudSyncCannotCreateRoot(NextcloudException):
	def __init__(self, *args):
		super().__init__("failed to create root directory", *args)


class NextcloudSyncCannotFetchRoot(NextcloudException):
	def __init__(self, *args):
		super().__init__("failed to fetch root directory", *args)


class NextcloudExceptionServerIsDown(NextcloudException):
	def __init__(self, *args):
		super().__init__(_("Unable to connect to the Nextcloud server"), *args)


class NextcloudExceptionInvalidCredentials(NextcloudException):
	def __init__(self, *args):
		super().__init__(_("Invalid username or password"), *args)


class NextcloudExceptionInvalidUrl(NextcloudException):
	def __init__(self, *args):
		msg = _("{0}: {1}").format(_("Nextcloud server URL"), _("Invalid URL"))
		super().__init__(msg, *args)
