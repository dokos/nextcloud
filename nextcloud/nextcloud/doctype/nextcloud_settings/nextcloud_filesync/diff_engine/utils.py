from functools import lru_cache

import frappe


def maybe_int(v: str | None) -> int | None:
	if isinstance(v, str) and v != "":
		return int(v)
	if isinstance(v, int):
		return v
	return None


# Environment variables:
# NEXTCLOUD_DONT_VERIFY_CERTS
# NEXTCLOUD_FORCE_VERIFY_CERTS
# NEXTCLOUD_ALLOW_TESTS
# NEXTCLOUD_SKIP_TESTS

FLAG_NEXTCLOUD_DISABLE_HOOKS = "nextcloud_disable_filesync_hooks"
FLAG_NEXTCLOUD_IGNORE = "nextcloud_triggered_update"


def set_flag(doc):
	doc.flags.nextcloud_triggered_update = True


def check_flag(doc):
	return doc.flags.nextcloud_triggered_update


def check_is_local_file(doc):
	if doc.is_folder:
		return True

	# To upload a document, it should have a valid file_url:
	# - non empty (file_url != '')
	# - not an external link (i.e. should start with /)

	# NOTE: Is it useful to further check for /private/files/... or /files/... ?
	return doc.file_url and doc.file_url.startswith('/')


def get_home_folder():
	return frappe.get_doc("File", {"is_home_folder": 1})


@lru_cache(maxsize=None)
def get_home_folder_name() -> str:
	docname = frappe.db.get_value("File", {"is_home_folder": 1})
	if not docname:
		raise frappe.exceptions.DoesNotExistError("missing home folder")
	return docname


def doc_has_nextcloud_id(doc):
	i = doc.get("nextcloud_id", None)
	if i in (None, ""):
		return False
	return True


def doc_has_content(doc):
	content = doc.get('content', None)
	return isinstance(content, (str, bytes))
