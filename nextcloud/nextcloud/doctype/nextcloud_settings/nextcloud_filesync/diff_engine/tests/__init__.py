from .test_actions import *  # noqa
from .test_diff_abstract import *  # noqa
from .test_diff_real import *  # noqa
from .test_fetcher import *  # noqa
from .test_idempotence import *  # noqa
from .test_other import *  # noqa
