import json
import os
from contextlib import contextmanager
from dataclasses import dataclass
from datetime import datetime
from operator import attrgetter
from typing import Generator, Literal, Optional

from owncloud.owncloud import FileInfo, HTTPResponseError

import frappe
from frappe.core.doctype.file.file import File
from nextcloud.nextcloud.doctype.nextcloud_settings import (
	NextcloudSettings,
	get_nextcloud_settings,
)
from nextcloud.nextcloud.doctype.nextcloud_settings.exceptions import (
	NextcloudException,
	NextcloudSyncMissingRoot,
)
from frappe.utils.data import cstr

from .diff_engine import ActionRunner, Common, DiffEngine, RemoteFetcher
from .diff_engine.Action import Action
from .diff_engine.ConflictTracker import ConflictResolverNCF, ConflictStopper
from .diff_engine.Entry import Entry, EntryRemote
from .diff_engine.utils import check_flag, check_is_local_file, doc_has_content, doc_has_nextcloud_id, get_home_folder, get_home_folder_name, set_flag
from .diff_engine.utils_time import convert_local_time_to_utc

logger = frappe.logger("nextcloud", allow_site=True, file_count=50)
logger.setLevel("DEBUG")


@dataclass
class BeforeSyncStatus:
	status: Literal["ok", "error"]
	sync_type: Literal["none", "normal", "all", "initial", "overwrite"] = "none"
	message: str = ""


@contextmanager
def sync_module(
	*, commit_after=True, raise_exception=True, rollback_on_exception=True
) -> Generator["NextcloudFileSync", None, None]:
	# frappe.db.begin()  # begin transaction
	# frappe.db.commit()  # begin transaction
	try:
		settings = get_nextcloud_settings()
		sync = NextcloudFileSync(settings=settings)
		yield sync
		sync.client.logout()

		if commit_after:
			frappe.db.commit()
	except Exception:
		if rollback_on_exception:
			frappe.db.rollback()  # rollback on error
		if raise_exception:
			raise


@contextmanager
def optional_sync_module(**kwargs) -> Generator[Optional["NextcloudFileSync"], None, None]:
	settings = get_nextcloud_settings()

	if frappe.flags.nextcloud_disable_filesync:
		yield None
		return

	if not (settings.enabled and settings.enable_sync):
		yield None
		return

	with sync_module(**kwargs) as syncer:
		yield syncer


def sync_log(*args):
	s = " ".join(map(str, args))

	if frappe.flags.print_nextcloud_log:
		print("nextcloud:", s)

	logger.info(s)


class NextcloudCustomJsonEncoder(json.JSONEncoder):
	def default(self, obj):
		from dataclasses import is_dataclass

		if isinstance(obj, Entry):
			return obj.toJSON()
		elif isinstance(obj, FileInfo):
			return f"FileInfo<{obj.path}>"
		elif isinstance(obj, datetime):
			return obj.isoformat()
		elif is_dataclass(obj):
			return obj.__dict__
		else:
			return json.JSONEncoder.default(self, obj)


def J(x):
	return json.dumps(x, cls=NextcloudCustomJsonEncoder, indent=2)


# def frappe_realtime_tqdm(iterable, desc: callable, count: int=None):
# 	if count is None and hasattr(iterable, '__len__'):
# 		count = len(iterable)
# 	if count is None:
# 		count = 9999999999999
# 	for (i, x) in enumerate(iterable):
# 		frappe.publish_realtime("progress", {
# 			'progress': [i + 1, count],
# 			'title': 'Cleaning up',
# 			'description': desc(x),
# 		}, task_id='sync', user=frappe.session.user)
# 		yield x


class NextcloudFileSync:
	def __init__(self, settings: NextcloudSettings):
		# TODO: Il faut que la première synchronisation soit [peut-être] faite manuellement ?
		# car très longue

		# TODO: Au-delà de 5000 fichiers (500 secondes) à synchroniser, on peut alerter l'utilisateur
		# pour le prévenir du risque de performances diminuées.
		# ET que la synchronisation sera pas complète avant un certain temps.

		# Mapping Dossier --> User (implicitement avec le nom du dossier)
		# TODO: ne synchroniser que les utilisateurs (email) existants
		# TODO: quand utilisateur créé -> synchro dossier vers utilisateur
		# « Mon dossier n'existe pas dans Dodock » --> Indiquer qu'il faut créer un utilisateur.

		# frappe.log_error()

		self.settings = settings

		self.client = self.settings.nc_connect()

		self.common = Common(
			client=self.client,
			settings=self.settings,
			logger=self.log,
		)

		self.runner = ActionRunner(self.common)

		self.fetcher = RemoteFetcher(self.common)

		self.conflicts: list[Action] = []

		self.set_conflict_strategy("ignore")

	def set_conflict_strategy(self, new_conflict_strategy: str):
		self.conflict_strategy = new_conflict_strategy

		use_conflict_detection = True  # pessimistic
		diff_even_when_conflict = False  # optimisation

		if self.conflict_strategy == "resolver-only":
			# emit conflicts, but leave their resolution to the resolver
			use_conflict_detection, diff_even_when_conflict = True, False
		elif self.conflict_strategy == "resolve-from-diff":
			# emit conflicts, but also emit corrective actions from differ
			use_conflict_detection, diff_even_when_conflict = True, True
		elif self.conflict_strategy == "UNSAFE-disable-conflict-detection":
			# emit conflicts, but also emit corrective actions from differ
			use_conflict_detection, diff_even_when_conflict = False, True
		else:
			use_conflict_detection, diff_even_when_conflict = True, False

		self.differ = DiffEngine(
			self.common,
			use_conflict_detection=use_conflict_detection,
			continue_diffing_after_conflict=diff_even_when_conflict,
		)

	def complete_sync(
		self,
		conflict_strategy: str = None,
		down_sync_all: bool = False,
		force_upload: bool = False,
		interactive: bool = False,
		commit: bool = True,
		rollback_on_error: bool = True,
	):
		"""
		Do a complete sync, including uploading unsynced files, logging conflicts to the user, database commit/rollback.

		Args:
		        conflict_strategy (str): The strategy to use for conflict resolution. Defaults to None (aka 'ignore').
		        down_sync_all (bool): Detect files even if their modification date is prior to the last sync. Defaults to False.

		        force_upload (bool): Clear all the nextcloud_id's of the local Files before uploading them again. Defaults to False.

		        interactive (bool): Display real time messages to the user. Defaults to False.
		        commit (bool): Defaults to True.
		        rollback_on_error (bool): Defaults to True.
		"""

		self.log(
			f"complete_sync(conflict_strategy={conflict_strategy}, down_sync_all={down_sync_all}, force_upload={force_upload})"
		)

		if not self.can_run_filesync():
			self.log("↳ complete_sync cancelled")
			return

		if conflict_strategy:
			self.set_conflict_strategy(conflict_strategy)

		try:
			# Perform a pre-sync migration to the cloud
			self.migrate_to_remote(force=force_upload)

			# Perform the sync, mirroring the cloud
			if down_sync_all:
				self.sync_from_remote_all()
			else:
				self.sync_from_remote_since_last_update()

			# Log conflicts to the user
			if interactive and self.conflicts:
				frappe.msgprint(
					frappe._(
						"Some conflicts were found during the Nextcloud sync, check the Error Log for more information."
					)
				)

			# success
			if commit:
				frappe.db.commit()
		except Exception:
			if rollback_on_error:
				frappe.db.rollback()
			raise

	def preserve_remote_root_bak(self):
		self.log("* Creating backup of remote root dir...")
		bak = self.common.root.rstrip("/") + ".bak"

		try:
			self.client.delete(bak)
			self.log("↳ deleted previous backup")
		except HTTPResponseError as e:
			# 404 is expected, but not other status codes
			if e.status_code != 404:
				raise

		try:
			self.client.move(self.common.root, bak)
			self.log("↳ created backup by renaming")
		except HTTPResponseError as e:
			# 404 is expected, but not other status codes
			if e.status_code != 404:
				raise

		self.log("↳ done: " + bak)
		return True

	def _log(self, *args):
		sync_log(*args)

	def log(self, *args):
		self._log(*args)

	def _fetch(self, last_update_dt: datetime = None):
		if last_update_dt is None:
			files = self.fetcher.fetch_all()
		else:
			dt_utc = convert_local_time_to_utc(last_update_dt)
			files = self.fetcher.fetch_since_utc(dt_utc)

		remote_entries = list(map(self.common.convert_remote_file_to_entry, files))
		remote_entries.sort(key=attrgetter("path"))
		return remote_entries

	def _sync(self, last_update_dt: datetime = None):
		sync_start_dt = frappe.utils.now_datetime()

		self.log(
			f'Sync all {last_update_dt.strftime("updated since %F %X (local time)") if last_update_dt else "from scratch"}'
		)

		# fetch all remote entries
		remote_entries = self._fetch(last_update_dt)
		self.log(f"fetched {len(remote_entries)} remote entries:", J(remote_entries))

		if len(remote_entries) == 0:
			# this case will never be run,
			# since the root dir is always returned
			self.log("nothing to do, stopping")
			return

		# initialize diffing
		# remote_entries = frappe_realtime_tqdm(remote_entries, lambda e: e.path)
		actions_iterator = self.differ.diff_from_remote(remote_entries)

		if self.conflict_strategy == "stop":
			conflict_stopper = ConflictStopper()
			actions_iterator = list(conflict_stopper.chain(actions_iterator))
			self.conflicts.extend(conflict_stopper._local_conflicts)
		elif self.conflict_strategy == "resolver-only":
			conflict_resolver = ConflictResolverNCF(self.common)
			actions_iterator = list(conflict_resolver.chain(actions_iterator))

		self.log(f"got {len(actions_iterator)} actions to run:", J(actions_iterator))

		self.log(f'running actions... (conflict strategy is "{self.conflict_strategy}")')

		# perform diffing + execute actions
		self.runner.run_actions(actions_iterator)

		# return the new value of the last_filesync_dt field of Nextcloud Settings
		newest_remote_entry = max(remote_entries, key=attrgetter("last_updated"))

		sync_duration = frappe.utils.now_datetime() - sync_start_dt
		self.log("all done in:", round(sync_duration.total_seconds(), 3), "seconds")
		self.log()

		last_update_dt2 = min(sync_start_dt, newest_remote_entry.last_updated)
		if last_update_dt is not None and (last_update_dt2 < last_update_dt):
			last_update_dt2 = last_update_dt  # can't be decreasing

		self.post_sync_success(last_update_dt2)

	def sync_from_remote_all(self):
		self._sync(last_update_dt=None)

	def sync_from_remote_since_last_update(self):
		self.sync_from_remote_since(self.settings.get_last_filesync_dt())

	def sync_from_remote_since(self, last_update_dt: datetime):
		self._sync(last_update_dt=last_update_dt)

	def _unjoin_all_files(self):
		"""Clear the `nextcloud_*` fields of all the local Files."""
		frappe.db.sql(
			"""
			update `tabFile` set
				`nextcloud_id` = NULL,
				`nextcloud_parent_id` = NULL,
				`nextcloud_etag` = NULL,
				`nextcloud_exclude` = 0
		"""
		)

	def migrate_to_remote(self, force=False, *, DANGER_clear_remote_files=False):
		if not self.can_run_filesync():
			return

		if DANGER_clear_remote_files:
			self.client.delete(self.common.root)

		if force:
			self._unjoin_all_files()

		# unsynced files are those that:
		# - have no nextcloud_id
		# - have been modified after the last sync

		home_docname = get_home_folder_name()
		query = {
			"or_filters": [
				("nextcloud_id", "=", ""),
				("modified", ">", self.settings.get_last_filesync_dt()),
				("name", "=", home_docname),
			],
			"filters": [
				("nextcloud_exclude", "=", False),
			],
			"order_by": "folder, file_name",
		}
		if self.settings.filesync_exclude_private:
			query["filters"].append(("is_private", "=", False))

		unsynced_files = frappe.get_all("File", **query)

		for f in unsynced_files:
			name = f["name"]
			doc = frappe.get_doc("File", name)
			self._upload_to_remote(doc, event="sync")

	def _upload_to_remote(self, doc: File, event=None):
		self.log(f"* upload to remote: {str(doc)} (event={event})")

		if check_flag(doc):
			self.log("↳ skipping, flag nextcloud_triggered_update is set")
			return  # avoid recursive updates

		if not check_is_local_file(doc):
			self.log("↳ skipping, file_url is external link")
			return

		if doc.nextcloud_exclude:
			self.log("↳ skipping, file has nextcloud_exclude=True")
			return

		if self.settings.filesync_exclude_private:
			if doc.is_private:
				self.log("↳ skipping, file is private")
				return  # skipping private files

		self._create_or_force_update_doc_in_remote(doc, check_remote=True)

	def file_on_trash(self, doc: File):
		self.log(f"* DELETE: {str(doc)}")
		if not self.can_run_filesync(doc):
			return

		if not doc_has_nextcloud_id(doc):
			self.log("↳ skipping: no nextcloud_id")
			return

		try:
			self._delete_remote_file_of_doc(
				doc,
				update_doc=False,  # don't need to update document, because it will be deleted
			)
		except Exception:
			return

	def _delete_remote_file_of_doc(
		self,
		doc: File,
		*,
		update_doc=False,
	):
		nextcloud_id = doc.nextcloud_id
		try:  # remove remote file
			remote_file = self.client.file_info_by_fileid(nextcloud_id)
			assert remote_file is not None
			self.client.delete(remote_file.path)
			self.log(f"↳ okay {str(doc)} ({nextcloud_id}@nextcloud): removed nextcloud file")
		except Exception as e:
			self.log(f"↳ fail {str(doc)} ({nextcloud_id}@nextcloud): failed to remove nextcloud file")
			self.log(e)
			raise

		if update_doc:
			doc.db_set(
				{
					"nextcloud_id": None,
					"nextcloud_parent_id": None,
					"nextcloud_etag": None,
				}
			)
			self.log(f"↳ updated {str(doc)} to remove nextcloud_id")
			set_flag(doc)

	def _update_etag_for_id(self, nextcloud_id: int):
		local = self.common.get_local_entry_by_id(nextcloud_id)
		remote = self.common.get_remote_entry_by_id(nextcloud_id)
		if local and remote:
			self.runner.run_actions([Action(type="meta.updateEtag", remote=remote, local=local)])

	def file_on_create(self, doc: File):
		self.log(f"* CREATE: {str(doc)}")
		if not self.can_run_filesync(doc):
			return

		if doc_has_nextcloud_id(doc):
			raise ValueError("File already has a nextcloud_id")

		if not check_is_local_file(doc):
			self.log("↳ skipping, file_url is external link")
			return

		self._create_or_force_update_doc_in_remote(doc, check_remote=True)
		set_flag(doc)

	def file_on_update(self, doc: File):
		self.log(f"* UPDATE: {str(doc)}")
		if not self.can_run_filesync(doc, ignore_private_check=True):
			return

		should_stop_syncing = False

		if doc.is_private and self.settings.filesync_exclude_private:
			should_stop_syncing = True

		# NOTE: don't forget to add ignore_exclude_check=True to self.can_run_filesync
		# if doc.nextcloud_exclude:
		# 	should_stop_syncing = True

		if should_stop_syncing:
			self.log("↳ checking if private/excluded file was public/included…")
			# We should not update this private/excluded file, but...

			# if the file has been synced previously
			was_synced = doc_has_nextcloud_id(doc)
			if not was_synced:
				return self.log(" → skip (not synced)")

			# and if the file WAS public/included
			prev_doc: dict = doc.get_doc_before_save() or {}
			was_public = not prev_doc.get("is_private", False)
			# was_included = not prev_doc.get("nextcloud_exclude", False)

			# if not was_public or not was_included:
			if not was_public:
				return self.log(" → skip (was not public/included)")

			# then it is BECOMING private/excluded
			# so, it must be deleted from the remote
			# because it might have been uploaded before.
			self.log("↳ file is becoming private/excluded: delete from remote")
			self._delete_remote_file_of_doc(doc, update_doc=True)
			return

		if not self.can_run_filesync(doc):
			return  # maybe not needed

		if not check_is_local_file(doc):
			self.log("↳ skipping, file_url is external link")
			return

		self._create_or_force_update_doc_in_remote(doc, check_remote=True)

	def can_run_filesync(self, doc: File = None, *, ignore_private_check=False, ignore_exclude_check=False, is_hook=False):
		if not self.settings.enabled:
			self.log("↳ WILL NOT RUN: nextcloud integration disabled")
			return False

		if not self.settings.enable_sync:
			self.log("↳ WILL NOT RUN: file sync disabled")
			return False

		if frappe.flags.nextcloud_disable_filesync:
			self.log("↳ WILL NOT RUN: file sync disabled by flag")
			return False

		if is_hook and frappe.flags.nextcloud_disable_filesync_hooks:
			self.log("↳ WILL NOT RUN: all hooks disabled")
			return False

		if doc:
			if check_flag(doc):
				# skipping because the file was already handled
				self.log("↳ WILL NOT RUN: flag is set on file", doc)
				return False

			if doc.nextcloud_exclude and not ignore_exclude_check:
				# skipping because the file is excluded
				self.log("↳ WILL NOT RUN: file has `nextcloud_exclude` checked", doc)
				return False

			if self.settings.filesync_exclude_private and not ignore_private_check:
				if doc.is_private:  # skipping private files
					self.log("↳ WILL NOT RUN: file is private", doc)
					return False

		return True

	# TODO
	def _deduplicate(self, main_doc: File, duplicates: str, remote: EntryRemote):
		if isinstance(duplicates, (map, filter)):
			duplicates = list(duplicates)

		if not isinstance(duplicates, list):
			raise ValueError("Nextcloud Sync: _deduplicate: param `duplicates` must be a list")

		if not all(map(lambda d: isinstance(d, str), duplicates)):
			raise ValueError("Nextcloud Sync: _deduplicate: param list `duplicates` should only contain strings (names of documents)")

		if not duplicates:
			# There are no duplicates, thus it's not possible to dedup them
			return

		if not main_doc:
			# main_doc = frappe.get_doc("File", duplicates[0])
			raise ValueError("Nextcloud Sync: _deduplicate: please provide a main_doc")

		if not main_doc.name:
			raise ValueError("Nextcloud Sync: _deduplicate: main_doc should have a name")

		# Eventually filter out the main_doc from the list of duplicates
		duplicates = [
			docname
			for docname in duplicates
			if docname != main_doc.name
		]

		if len(duplicates) == 0:
			return

		self.log('Deduplicating', duplicates, ' -> Main document is:', main_doc)

		if main_doc.nextcloud_exclude:
			self.log("Nextcloud Sync: _deduplicate: main_doc must not have nextcloud_exclude set to True")
			# No need to db_set, the file is being created/updated
			main_doc.nextcloud_exclude = False

		for name in duplicates:
			dupl_doc: File = frappe.get_doc("File", name)

			dupl_doc.add_comment(text=frappe._("Nextcloud: This file is in the same folder as a file with the same name. It has been automatically excluded from the synchronization as it is not possible to upload distinct files with the same names in the same folder."))

			set_flag(dupl_doc)  # skip sync of this file in the current sync

			# name, ext = os.path.splitext(dupl_doc.file_name)
			# dupl_doc.file_name = name + f" ({i + 1})" + ext

			# TODO: should we use the exclusion mecanism for that?
			dupl_doc.db_set({
				# Exclude the file from sync
				"nextcloud_exclude": True,
				# "nextcloud_duplicate_of": main_doc.name

				# Clear up all Nextcloud fields
				"nextcloud_id": None,
				"nextcloud_etag": None,
				"nextcloud_parent_id": None,
			}, update_modified=False)

			# NOTE: The system do not update the "modified" date.
			# What about user-overwrite? If an user saves the form of a File
			# that was silently updated by the duplicate-avoidance system,
			# then it might not have any negative effect.
			# Indeed, if the updated file is truly a duplicate, then it becomes
			# the main_doc and will be pushed to the server. This might be what
			# the user intended also, because there is currently no way for an
			# user to know of duplicates.


	# TODO
	def _get_duplicates(self, doc: File, remote: EntryRemote):
		"""
		This function returns a list of docnames of File documents that share
		the same remote path on the Nextcloud server. Note that the returned
		list WILL contain the name of the `doc` parameter.

		When a file is created locally, it overwrites the remote file.
		However, the remote file's nextcloud_id does not change when being overwritten.
		It means that, if multiple local Files have the same file_name and folder,
		then they are associated to the same remote path. Therefore, multiple local
		files can have the same associated remote file, which might become a problem
		if we want to update the local files based on the remote file.

		What we can check:
			- do the duplicates share the same nextcloud_id?
			- do the duplicates share the same (folder, file_name)?
			- do the duplicates share the same content_hash?
			- do the duplicates share the same file_url?

		The main problem is when multiple local files share the same remote path,
		that is when they have the same (folder, file_name) values.
		If it is the case, then either they are strictly identical and shoud either:
			- be joined to the same remote file (with the same Nextcloud ID, Parent ID,
			ETag and modification time), or they should 


		1. Different content_hash's but same file_url.
		Something has gone wrong in Frappe/Dodock.

		| PATH NCID HASH URLS
		| *    *    !=   eq


		2. Same content_hash and same file_url.
		All have the same data file.

		| PATH NCID HASH URLS
		| eq   eq   eq   eq   -> true duplicate
		| eq   !=   eq   eq   -> duplicate with misplaced duplicate remote file (might cause problems?)
		| !=   eq   eq   eq   -> impossible (same ID on different paths should disappear after sync)
		| !=   !=   eq   eq   -> duplicate with extra remote file (might cause problems?)


		3. Same content_hash but different file_url's.
		The actual content is the same, but in multiple data files.
		It's weird that the URLs are different.

		| PATH NCID HASH URLS
		| eq   eq   eq   !=   -> remote collision (because same remote ID)
		| eq   !=   eq   !=   -> remote collision (because same remote path)
		| !=   eq   eq   !=   -> impossible: same ID but different paths
		| !=   !=   eq   !=   -> okay, but impossible (data files are distinct even if they are identical)


		4. Differents content_hash's and different file_url's.

		| PATH NCID HASH URLS
		| eq   eq   !=   !=   -> hard collision (impossible to handle correctly)
		| eq   !=   !=   !=   -> impossible (same path for different IDs should disappear after sync-pull)
		| !=   eq   !=   !=   -> impossible: same ID but different paths (failed earlier rename?)
		| !=   !=   !=   !=   -> okay (distinct files)
		"""

		# TODO: write a function that returns filters and or_filters to query local files that should get synced (is_private...)

		assert doc.name
		assert doc.file_name

		if doc.is_folder:
			# It is not possible for a folder to have duplicates
			# because of the uniqueness constraint of the `name` field.
			return None

		if doc.nextcloud_exclude:
			self.log("Nextcloud Sync: _get_duplicates: warn: `doc` has nextcloud_exclude set to True")

		# Build up the query
		query = {
			'filters': [
				# Get files that have a path collision. It's the most important criteria,
				# because it would not be really helpful to deduplicate files that would
				# not overwrite each other on the remote server.
				('folder', '=', str(doc.folder)),
				('file_name', '=', str(doc.file_name)),

				# Exclude the doc that is used to query the DB.
				# Note that it will be included again anyways.
				# ('name', '!=', str(doc.name)),

				# Ignore excluded files, they will not cause problems.
				('nextcloud_exclude', '=', False),
			],
			# 'order_by': 'creation desc',  # get oldest first
		}

		if self.settings.filesync_exclude_private:
			query['filters'].append(('is_private', '=', False))

		# query['filters'].append(('owner', '=', doc.owner))

		# Execute the query
		duplicates = frappe.get_all("File", **query)  # type: List[str]

		# Only return the names of the duplicates as strings
		duplicates = [
			x if isinstance(x, str) else x.name
			for x in duplicates
		]

		if doc.name and doc.name not in duplicates:
			self.log("Nextcloud Sync: _get_duplicates: warn: `doc` used to query the DB for collisions was not returned by the query.")
			duplicates.insert(0, doc.name)  # prepend

		return duplicates

	def _create_or_force_update_doc_in_remote(self, doc: File, check_remote: bool = False):
		"""
		Upload the local file to the remote.
		If `check_remote` is True, try to find the matching remote file first, in order to perform a smarter sync if possible.
		"""
		local = self.common.convert_local_doc_to_entry(doc)

		self.log("*** _create_or_force_update_doc_in_remote", doc, check_remote)

		remote = None
		if check_remote:
			# Try to find the remote file by its ID
			if doc_has_nextcloud_id(doc):
				remote = self.common.get_remote_entry_by_id(doc.nextcloud_id)

			# Else, try using its full path
			if remote is None:
				remote = self.common.get_remote_entry_by_path(local.path)

		if remote:
			# Check that the remote is of the same type (file vs. folder) as the local.
			# if local.is_dir() != remote.is_dir():
			if local.is_dir() and not remote.is_dir():
				raise ValueError(frappe._("Impossible to upload this folder to Nextcloud: a normal file of the same name already exists on the Nextcloud server."))
			if remote.is_dir() and not local.is_dir():
				raise ValueError(frappe._("Impossible to upload this file to Nextcloud: a folder of the same name already exists on the Nextcloud server."))

			# TODO: Correctly handle the case where multiple identical duplicates exist.
			# TODO: Handle the case where Files have the same remote path but different contents.
			duplicates: list = self._get_duplicates(doc, remote)
			if duplicates:
				self._deduplicate(doc, duplicates, remote)

		try:
			if doc.is_folder or not doc_has_content(doc):
				return self.runner.run_actions(
					[Action(type="remote.createOrUpdate", local=local, remote=remote)]
				)

			self.runner.run_actions([Action(type="remote.createOrForceUpdate", local=local, remote=remote)])
		except HTTPResponseError as e:
			if e.status_code == 507:
				self.log("Insufficient Storage", local, remote)
				raise

			if e.status_code == 409 or e.status_code == 404 or e.status_code == 405:
				# The remote parent dir is missing,
				# so we try to create the parent hierarchy.
				# The system could also just wait for the next sync to upload the missing files.
				self.log(e)
				self.log("Failed to do remote.createOrForceUpdate", local, remote)
				self.log("Trying by first creating parent hierarchy")

				actions = []
				cur_doc = doc
				loop_guard = 10

				while cur_doc and cur_doc.folder not in ("", None):
					l = self.common.convert_local_doc_to_entry(cur_doc)
					r = None

					if l.nextcloud_id:
						r = self.common.get_remote_entry_by_id(l.nextcloud_id)

					if not r:
						r = self.common.get_remote_entry_by_path(l.path)

					if r:
						break

					a = Action(type="remote.createOrForceUpdate", local=l, remote=None)
					actions.append(a)

					loop_guard -= 1
					if loop_guard < 0:
						raise NextcloudException(
							"Failed to create hierarchy after failed remote.createOrForceUpdate: hierarchy is deeper than 10 directories"
						) from e

					cur_doc = frappe.get_doc("File", cur_doc.folder)

				actions.reverse()
				self.log(" * " + "\n * ".join(map(repr, actions)))
				self.runner.run_actions(actions)
			else:
				raise

	def get_before_sync_status(self):
		if not self.can_run_filesync():
			return BeforeSyncStatus("ok", "none", message="disabled")

		# Fetch local and remote root directories
		try:
			local_home = get_home_folder()
		except frappe.DoesNotExistError:
			raise  # TODO: this is bad

		local_id = cstr(local_home.nextcloud_id)

		try:
			remote_home = self.fetcher.fetch_root(create_if_missing=False)
		except NextcloudSyncMissingRoot:
			remote_home = None

		# If the remote dir does not exist
		if not remote_home:
			if local_id:
				potential_remote_home = self.client.file_info_by_fileid(local_id)
				if potential_remote_home:
					print("RENAMED", self.settings.path_to_files_folder, "->", potential_remote_home.path)
					self.settings.db_set("path_to_files_folder", potential_remote_home.path)
					# The remote root directory was moved/renamed.
					# We have to make a choice here:

					# 1. We can ignore the fact that a remote directory
					# already exists and pursue with a force-upload,
					# creating a new remote directory. In this case,
					# data loss can NOT happen, data duplication might.

					# 2. Or we can adjust the `path_to_files_folder` value
					# by assuming that the users wanted to rename the folder
					# but keep it synchronized. In this case, data loss CAN
					# happen on the local files, because most local files
					# are expected to not be uploaded before syncing, which
					# means that, in the case of a wrong rename detection,
					# a possibly empty remote dir might get synced to the
					# local files.
					# return BeforeSyncStatus('ok', ['adjust-root', 'force-upload', 'down-sync'])
					return BeforeSyncStatus("ok", "initial")

			# Knowing that the Home directory is not synced is not enough
			# to be sure that all its local children are also not synced.
			# Simply soft-uploading the files, some of which could have a
			# nextcloud_id, possibly a remote mirror, would not be enough
			# to correctly copy all the local files to the remote server.
			# So, the files have to be force-uploaded.
			# return BeforeSyncStatus('ok', ['force-upload', 'down-sync'])
			return BeforeSyncStatus("ok", "initial")

		# Both local and remote dirs exist
		# Detect if they are correctly linked
		if not local_id:
			# Local and remote roots are not linked.
			# But the remote root dir exists, which means that an up-sync
			# COULD lead to data loss (by overwrite) on the remote files.
			# remote_is_empty = len(self.client.list(remote_home.path))
			# return BeforeSyncStatus('ok', ['up-sync', 'down-sync'])
			return BeforeSyncStatus("ok", "normal")

		remote_id = cstr(remote_home.attributes[self.common._FILE_ID])
		if local_id != remote_id:
			# return BeforeSyncStatus('error', message='home-different-ids')
			return BeforeSyncStatus("ok", "overwrite")

		# return BeforeSyncStatus('ok', ['up-sync', 'down-sync'])
		return BeforeSyncStatus("ok", "normal")

	def post_sync_success(self, last_update_dt2):
		self.settings.set_last_filesync_dt(dt_local=last_update_dt2)

