import frappe
import datetime
from nextcloud.nextcloud.doctype.nextcloud_settings.nextcloud_filesync.diff_engine.Action import (
	Action,
)
from nextcloud.nextcloud.doctype.nextcloud_settings.nextcloud_filesync.diff_engine.tests._tester import (
	NextcloudTester,
	using_local_files,
	using_remote_files,
	using_syncer,
)

class TestSyncer(NextcloudTester):
	@using_remote_files(
		[
			"/test_missing_remote_parent/",
			"/test_missing_remote_parent/A/",
			"/test_missing_remote_parent/A/file",
			"/test_missing_remote_parent/A/dir/",
		]
	)
	@using_local_files(
		[
			dict(file_name="test_missing_remote_parent", folder="Home", is_folder=1),
			dict(file_name="A", folder="Home/test_missing_remote_parent", is_folder=1),
			dict(file_name="dir", folder="Home/test_missing_remote_parent/A", is_folder=1),
			dict(file_name="file", folder="Home/test_missing_remote_parent/A", content="z"),
		]
	)
	@using_syncer()
	def test_missing_remote_parent(self):
		prefix = "/test_missing_remote_parent/"
		local_children_folder = "Home/test_missing_remote_parent/A"

		def compare_hierarchies():
			def remove_prefix(path: str) -> str:
				if path.startswith(prefix):
					return "/" + path[len(prefix):]
				return path

			all_r, all_l = self.get_remote_and_local_files_as_lists(prefix)

			self.assertEqual(len(all_r), len(all_l))

			for r, l in zip(all_r, all_l):
				r_path, l_path = map(remove_prefix, (r.path, l.path))
				self.assertEqual(r_path, l_path)
				self.assertEqual(r.nextcloud_id, l.nextcloud_id)
				self.assertEqual(r.etag, l.etag)
				self.assertAlmostEqual(r.last_updated, l.last_updated, delta=datetime.timedelta(seconds=30))

		for path in self._remote_files:
			self.join(path)

		compare_hierarchies()

		self.remote_delete(prefix)

		children = frappe.get_all("File", {"folder": local_children_folder})

		for f in children:
			doc = frappe.get_doc("File", f)
			self.syncer._create_or_force_update_doc_in_remote(doc, False)

		for doc in self._local_files:
			l = self.common.convert_local_doc_to_entry(doc)
			r = self.common.get_remote_entry_by_path(l.path)
			if l.etag != r.etag:
				self.runner.run_actions([Action("meta.updateEtag", l, r)])

		compare_hierarchies()

	@using_remote_files(
		[
			"/test_handle_true_duplicates/",
			"/test_handle_true_duplicates/ORIGINAL",
		]
	)
	@using_local_files(
		[
			dict(file_name="test_handle_true_duplicates", folder="Home", is_folder=1),
			dict(file_name="ORIGINAL", folder="Home/test_handle_true_duplicates", content="abc"),
			dict(file_name="true_duplicate_1", folder="Home/test_handle_true_duplicates", content="abc"),
			dict(file_name="true_duplicate_2", folder="Home/test_handle_true_duplicates", content="abc"),
		]
	)
	@using_syncer()
	def test_duplicates_are_excluded_automatically(self):
		def get_doc_by_file_name(file_name):
			return frappe.get_doc("File", {
				"folder": "Home/test_handle_true_duplicates",
				"file_name": file_name,
			})

		# Join remote files to local ones
		for path in self._remote_files:
			self.join(path)

		# Basic assertion
		children = frappe.get_all("File", {"folder": "Home/test_handle_true_duplicates"})
		self.assertEqual(len(children), 3)

		# Retrieve files
		main_doc = get_doc_by_file_name("ORIGINAL")
		dupl_1 = get_doc_by_file_name("true_duplicate_1")
		dupl_2 = get_doc_by_file_name("true_duplicate_2")

		# Create collision
		for dupl in (dupl_1, dupl_2):
			for fieldname in ('folder', 'file_name'):
				dupl.set(fieldname, main_doc.get(fieldname))

		# Save the documents
		dupl_1.save()
		dupl_2.save()

		# Perform update to trigger the deduplication system
		self.syncer._create_or_force_update_doc_in_remote(main_doc, check_remote=True)

		# Reload the files (superfluous)
		main_doc.reload()
		dupl_1.reload()
		dupl_2.reload()

		# Assertions
		for dupl in (dupl_1, dupl_2):
			# 1. Duplicates should be excluded from the sync.
			self.assertTrue(dupl.nextcloud_exclude)

			# 2. Duplicates should have empty values for the
			# nextcloud_id, nextcloud_parent_id and nextcloud_etag fields.
			for fieldname in ('nextcloud_id', 'nextcloud_parent_id', 'nextcloud_etag'):
				self.assertFalse(dupl.get(fieldname, None))

	@using_remote_files(
		[
			"/test_deduplication_switch_main_doc/",
			"/test_deduplication_switch_main_doc/ORIGINAL",
		]
	)
	@using_local_files(
		[
			dict(file_name="test_deduplication_switch_main_doc", folder="Home", is_folder=1),
			dict(file_name="ORIGINAL", folder="Home/test_deduplication_switch_main_doc", content="abc"),
			dict(file_name="true_duplicate_1", folder="Home/test_deduplication_switch_main_doc", content="abc"),
			dict(file_name="true_duplicate_2", folder="Home/test_deduplication_switch_main_doc", content="abc"),
		]
	)
	@using_syncer()
	def test_deduplication_switch_main_doc(self):
		frappe.flags.print_nextcloud_log = True
		def get_doc_by_file_name(file_name):
			return frappe.get_doc("File", {
				"folder": "Home/test_deduplication_switch_main_doc",
				"file_name": file_name,
			})

		# Join remote files to local ones
		for path in self._remote_files:
			self.join(path)

		# Retrieve files
		main_doc = get_doc_by_file_name("ORIGINAL")
		dupl_1 = get_doc_by_file_name("true_duplicate_1")
		dupl_2 = get_doc_by_file_name("true_duplicate_2")

		# Create collision
		for dupl in (dupl_1, dupl_2):
			for fieldname in ('folder', 'file_name'):
				dupl.set(fieldname, main_doc.get(fieldname))

		# Save the documents
		dupl_1.save()
		dupl_2.save()

		for d in (main_doc, dupl_1, dupl_2):
			print(d.as_dict())

		# Perform update to trigger the deduplication system
		self.syncer._create_or_force_update_doc_in_remote(main_doc, check_remote=True)

		# Perform update on excluded file to switch the main doc
		self.syncer._create_or_force_update_doc_in_remote(dupl_1, check_remote=True)

		# Reload the files (superfluous)
		main_doc.reload()
		dupl_1.reload()
		dupl_2.reload()

		for d in (main_doc, dupl_1, dupl_2):
			print(d.as_dict())

		# Assertions
		for dupl in (main_doc, dupl_2):
			# 1. Duplicates should be excluded from the sync.
			self.assertTrue(dupl.nextcloud_exclude)

			# 2. Duplicates should have empty values for the
			# nextcloud_id, nextcloud_parent_id and nextcloud_etag fields.
			for fieldname in ('nextcloud_id', 'nextcloud_parent_id', 'nextcloud_etag'):
				self.assertFalse(dupl.get(fieldname, None))


	@using_remote_files(
		[
			"/test_handle_id_duplicates/",
			"/test_handle_id_duplicates/ORIGINAL",
		]
	)
	@using_local_files(
		[
			dict(file_name="test_handle_id_duplicates", folder="Home", is_folder=1),
			dict(file_name="ORIGINAL", folder="Home/test_handle_id_duplicates", content="abc"),
			dict(file_name="id_duplicate_1", folder="Home/test_handle_id_duplicates", content="abc"),
		]
	)
	@using_syncer()
	def test_nextcloud_id_field_is_unique(self):
		def get_doc_by_file_name(file_name):
			return frappe.get_doc("File", {
				"folder": "Home/test_handle_id_duplicates",
				"file_name": file_name,
			})

		# Join remote files to local ones
		for path in self._remote_files:
			self.join(path)

		# Retrieve files
		main_doc = get_doc_by_file_name("ORIGINAL")
		dupl_1 = get_doc_by_file_name("id_duplicate_1")

		# Create collision
		dupl_1.nextcloud_id = main_doc.nextcloud_id

		# Save the document and check that an exception is thrown
		with self.assertRaises(frappe.exceptions.UniqueValidationError):
			dupl_1.save()
