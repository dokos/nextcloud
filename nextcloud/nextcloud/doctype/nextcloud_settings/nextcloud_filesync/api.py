import frappe
from frappe import _
from nextcloud.nextcloud.doctype.nextcloud_settings import get_nextcloud_settings
from nextcloud.nextcloud.doctype.nextcloud_settings.exceptions import (
	NextcloudExceptionInvalidCredentials,
	NextcloudExceptionInvalidUrl,
	NextcloudExceptionServerIsDown,
)

from .diff_engine.utils import check_flag, doc_has_nextcloud_id
from .sync import optional_sync_module, sync_log, sync_module


@frappe.whitelist()
def check_server():
	err_title = _("Nextcloud")

	try:
		settings = get_nextcloud_settings()
		if settings.enabled:
			settings.nc_connect()
			# no exception raised
		return {"status": "ok"}
	except (NextcloudExceptionInvalidUrl, NextcloudExceptionServerIsDown, NextcloudExceptionInvalidCredentials) as e:
		# frappe.clear_messages()
		frappe.msgprint(str(e.args[0]), title=err_title)
		return {"status": "error"}


@frappe.whitelist()
def sync_from_remote_all():
	with optional_sync_module() as syncer:
		if not syncer:
			return "skip"
		syncer.sync_from_remote_all()


@frappe.whitelist()
def sync_from_remote_since_last_update():
	with optional_sync_module() as syncer:
		if not syncer:
			return "skip"
		syncer.sync_from_remote_since_last_update()


# HOOKS
def can_run_hook(doc=None):
	if frappe.flags.nextcloud_disable_filesync_hooks:
		sync_log("↳ skipping hook: all nc hooks disabled")
		return False
	if frappe.flags.nextcloud_disable_filesync:
		sync_log("↳ skipping hook: file sync disabled")
		return False
	if doc and check_flag(doc):
		sync_log("↳ skipping hook: flag is set on file", doc)
		return False
	return True


@frappe.whitelist()
def file_on_trash(doc, event):
	sync_log(f"file_on_trash({str(doc)}, {event}, nc_id={doc.nextcloud_id})")
	if not can_run_hook(doc):
		return
	if not doc_has_nextcloud_id(doc):
		return  # not linked to remote
	if doc.flags.in_parent_delete:
		# sync_log('File on_trash Nextcloud hook: skipping, parent is already being deleted on remote')
		return

	try:
		with sync_module(rollback_on_exception=False) as syncer:
			syncer.file_on_trash(doc)

	except Exception as e:
		sync_log(e)
		return


@frappe.whitelist()
def file_on_update(doc, event):
	sync_log(f"file_on_update({str(doc)}, {event})")
	if not can_run_hook(doc):
		return

	# <TODO> move all this code inside of Syncer or ActionRunner…
	if frappe.flags.nextcloud_in_rename:
		# ignore: children are all "needlessly" updated
		# because of the parent's name being updated
		prev_doc = doc.get_doc_before_save()
		if prev_doc:
			modified = prev_doc.modified

			with sync_module(rollback_on_exception=False) as syncer:
				entry = syncer.common.get_remote_entry_by_id(doc.nextcloud_id)
				if entry:
					modified = entry.last_updated

			doc.db_set("modified", modified, update_modified=False)
			sync_log(f"corrected time of {str(doc)}: ", modified)
			frappe.db.commit()
		return
	# </TODO>

	try:
		with sync_module(rollback_on_exception=False) as syncer:
			syncer.file_on_update(doc)
	except Exception as e:
		sync_log(e)
		return


@frappe.whitelist()
def file_on_create(doc, event):
	sync_log(f"file_on_create({str(doc)}, {event})")
	if not can_run_hook(doc):
		return

	# if doc_has_nextcloud_id(doc):
	# 	return  # How did this file get a nextcloud_id?

	try:
		with sync_module(rollback_on_exception=False) as syncer:
			syncer.file_on_create(doc)
	except Exception as e:
		sync_log(e)
		return
