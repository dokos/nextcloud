import os
import traceback

import click
import frappe

from frappe.commands import pass_context, get_site
from frappe.core.doctype.file.file import File


@click.group(name='nextcloud')
@pass_context
def nextcloud(ctx):
	frappe.connect(site=get_site(ctx))
	settings = frappe.get_single('Nextcloud Settings')
	if not settings.enabled:
		click.echo(click.style('Nextcloud integration is not enabled', dim=True), err=True)


@nextcloud.command('status')
@pass_context
def status(ctx):
	frappe.connect(site=get_site(ctx))
	settings = frappe.get_single('Nextcloud Settings')
	frappe.flags.print_nextcloud_log = True

	try:
		server_is_up = settings.nc_ping_server()
		if server_is_up:
			status = click.style('up', fg='green', bold=True)
			click.echo('Nextcloud server is ' + status)
		else:
			status = click.style('down', fg='red', bold=True)
			click.echo('Nextcloud server is ' + status)
			return 1
	except Exception as e:
		click.echo(''.join(traceback.format_exception(0, e, e.__traceback__)), err=True)
		click.echo(click.style(str(e), fg='red'), err=True)
		return 1

	try:
		client = settings.nc_connect()
		click.echo('Nextcloud login seems ' + click.style('correct', fg='green', bold=True))
	except Exception as e:
		click.echo(''.join(traceback.format_exception(0, e, e.__traceback__)), err=True)
		click.echo(click.style(str(e), fg='red'), err=True)
		click.echo('Nextcloud login is ' + click.style('incorrect', fg='red', bold=True))
		return 1

	try:
		client.file_info('/')
		click.echo('Nextcloud login is ' + click.style('correct', fg='green', bold=True))
	except Exception as e:
		click.echo(''.join(traceback.format_exception(0, e, e.__traceback__)), err=True)
		click.echo(click.style(str(e), fg='red'), err=True)
		click.echo('Nextcloud login is ' + click.style('incorrect', fg='red', bold=True))
		return 1


@nextcloud.command('settings')
@click.option('--show-password/--hide-password', is_flag=True, default=True)
@pass_context
def settings(ctx, show_password):
	frappe.connect(site=get_site(ctx))
	settings = frappe.get_single('Nextcloud Settings')

	username, password = settings._get_credentials()
	if not show_password:
		password = '*' * len(password)
	cloud_given_url = settings.cloud_url
	cloud_url = settings._get_cloud_base_url()

	sync_enabled = settings.enable_sync

	def echo_kv(key: str, value):
		click.echo((key + ': ').ljust(14) + click.style(value, bold=True))

	def echo_header(text: str):
		click.echo(click.style(text, bold=True, underline=True))

	def echo_faint_header(text: str):
		click.echo(click.style(text, dim=True, underline=True))

	echo_header('Nextcloud: general settings')
	echo_kv('User name', username)
	echo_kv('Password', password)
	echo_kv('Cloud URL', cloud_url)
	echo_kv('Cloud URL (full value)', cloud_given_url)
	click.echo()

	if sync_enabled:
		echo_header('Nextcloud: file sync settings')
		echo_kv('Root folder', settings.get_path_to_files_folder())
		echo_kv('Last sync', settings.last_filesync_dt)
	else:
		echo_faint_header('File Sync is disabled')

	import json
	click.echo()
	click.echo('As a string to copy-paste into the app directly and restore settings:')
	settings.password = password
	click.echo(json.dumps(settings.as_dict()))


@nextcloud.command('sync')
@click.option('-a', '--all', 'arg_all', is_flag=True, default=False, help='Sync from scratch.')
@click.option('--since', 'arg_since', type=str, help="Sync since a given moment in time. Expected format is `yyyy-mm-dd hh:mm:ss`, local time.", default=None)
@pass_context
def sync(ctx, arg_since=None, arg_all=False):
	# check arguments and options
	if arg_since and arg_all:
		click.echo('Error: --all and --since flags are mutually exclusive', err=True)
		click.echo()
		with click.Context(sync) as ctx:
			click.echo(sync.get_help(ctx))
		click.echo()
		click.echo('Error: --all and --since flags are mutually exclusive', err=True)
		return 99

	frappe.connect(site=get_site(ctx))

	from nextcloud.nextcloud.doctype.nextcloud_settings.nextcloud_filesync.sync import sync_module
	frappe.flags.print_nextcloud_log = True
	with sync_module() as syncer:
		if syncer:
			if arg_all:
				syncer.sync_from_remote_all()
			elif arg_since:
				dt = frappe.utils.get_datetime(arg_since)
				dt = dt.astimezone(None).replace(tzinfo=None)
				syncer.sync_from_remote_since(dt)
			else:
				syncer.sync_from_remote_since_last_update()

@nextcloud.command('shell')
@pass_context
def shell(ctx):
	frappe.connect(site=get_site(ctx))

	from nextcloud.nextcloud.doctype.nextcloud_settings import get_nextcloud_settings_and_client
	from nextcloud.nextcloud.doctype.nextcloud_settings.nextcloud_filesync.sync import NextcloudFileSync
	client, settings = get_nextcloud_settings_and_client()
	sync = NextcloudFileSync(settings=settings)
	print('AVAILABLE VARIABLES: frappe, client, sync')
	import pdb; pdb.set_trace()

@nextcloud.command('cron')
@pass_context
def cron(ctx):
	frappe.connect(site=get_site(ctx))

	from nextcloud.nextcloud.doctype.nextcloud_settings.nextcloud_filesync.cron import run_cron
	frappe.flags.print_nextcloud_log = True
	run_cron()

@nextcloud.command('cron.status')
@pass_context
def cron_status(ctx):
	frappe.connect(site=get_site(ctx))

	from nextcloud.nextcloud.doctype.nextcloud_settings.nextcloud_filesync.cron import get_sync_module
	syncer = get_sync_module()
	print(syncer.get_before_sync_status())

@nextcloud.command('unjoin-all')
@pass_context
def unjoin_all(ctx):
	frappe.connect(site=get_site(ctx))

	from nextcloud.nextcloud.doctype.nextcloud_settings.nextcloud_filesync.sync import NextcloudFileSync
	frappe.flags.print_nextcloud_log = True
	NextcloudFileSync._unjoin_all_files(None)
	frappe.db.commit()
	print('done')

@nextcloud.command('rm')
@click.argument('nc_id', type=int)
@pass_context
def rm(ctx, nc_id):
	frappe.connect(site=get_site(ctx))

	filters={'nextcloud_id': nc_id}
	docname = frappe.db.exists('File', filters)
	if docname:
		frappe.delete_doc('File', docname)
		frappe.db.commit()
		print('✅')
	else:
		print('No File with nextcloud_id', nc_id)


@nextcloud.command('disable')
@pass_context
def disable(ctx):
	frappe.connect(site=get_site(ctx))
	frappe.get_single('Nextcloud Settings').db_set('enabled', 0)
	frappe.db.commit()


@nextcloud.command('enable')
@pass_context
def enable(ctx):
	frappe.connect(site=get_site(ctx))
	frappe.get_single('Nextcloud Settings').db_set({
		'enabled': 1,
		'debug_disable_filesync_cron': 0,
	})
	frappe.db.commit()


@nextcloud.command('ls-remote')
@pass_context
def ls_remote(ctx):
	frappe.connect(site=get_site(ctx))
	settings = frappe.get_single('Nextcloud Settings')
	client = settings.nc_connect()
	p = settings.get_path_to_files_folder()
	p = '/' + p.strip('/') + '/'
	files = [client.file_info(p, properties=client._QUERY_PROPS)]
	files += client.list(p, depth='infinity', properties=client._QUERY_PROPS)
	files.sort(key=lambda f: f.path)
	for f in files:
		print(
			f.path,
			f.attributes['{http://owncloud.org/ns}fileid'],
			f.attributes['{DAV:}getetag'],
		)


@nextcloud.command('ls-storage')
@pass_context
def ls_storage(ctx):
	frappe.connect(site=get_site(ctx))

	private_dir = frappe.get_site_path('private', 'files')
	public_dir = frappe.get_site_path('public', 'files')

	def show_file_hierarchy(base_path, prefix = ''):
		for root, dirs, files in os.walk(base_path):
			# if root.startswith(base_path):
			# 	root = root[len(base_path):]
			root = os.path.abspath(base_path) + root[len(base_path):]

			paths = [root + '/'] if root else []
			paths += [os.path.join(root, f) for f in files]
			for path in paths:
				if 'website_theme' in path:
					continue
				print(('📁' if path.endswith('/') else '📄') + prefix, path)

	show_file_hierarchy(public_dir, prefix='  ')
	show_file_hierarchy(private_dir, prefix='🔒')


@nextcloud.command('ls')
@pass_context
def ls(ctx):
	from nextcloud.nextcloud.doctype.nextcloud_settings.exceptions import NextcloudExceptionServerIsDown
	frappe.connect(site=get_site(ctx))

	all_files: list = frappe.get_all(
		'File',
		fields=['name', 'file_name', 'folder'])

	all_files.sort(
		key=lambda f: '/'.join(filter(None, (f['folder'], f['file_name'])))
	)

	try:
		client = frappe.get_single('Nextcloud Settings').nc_connect()
	except NextcloudExceptionServerIsDown:
		client = None

	for f in all_files:
		s = ''
		doc: File = frappe.get_doc('File', f['name'])

		if doc.is_folder:
			s += '📂'
		elif doc.file_url.startswith('/'):
			s += '📄'
		else:
			s += '🔗'

		if doc.is_private:
			s += '🔒'
		else:
			s += '  '

		_is_real_file = True
		_data_file = False
		_remote = False
		if not client:
			if doc.nextcloud_id:
				_remote = '?'
				s += '\x1b[34m'
		elif doc.nextcloud_id:
			_remote = client.file_info_by_fileid(doc.nextcloud_id)
			if _remote:
				s += '\x1b[32;1m'
			else:
				s += '\x1b[31;1m'

		if doc.is_folder or doc.exists_on_disk():
			_data_file = 'local'
		elif doc.file_url.startswith('/'):
			_data_file = False  # Missing
			s += '\x1b[35m'
		else:
			_data_file = False  # URL
			_is_real_file = False
			s += '\x1b[33;1m'

		s += '/'.join(filter(None, (doc.folder, doc.file_name)))

		if doc.nextcloud_id:
			s += f"\x1b[2m ({doc.nextcloud_id})"
			if not _remote:
				s += ' missing'
		s += '\x1b[m'

		if not doc.is_folder and doc.nextcloud_id and not _remote:
			s += ' \x1b[38;5;246m'
			s += f['name']
			s += '\x1b[m'

		if doc.nextcloud_etag and _remote and _remote != '?':
			remote_etag = _remote.get_etag()
			if remote_etag != doc.nextcloud_etag:
				s += f"\x1b[31;3m (ETAG! local={doc.nextcloud_etag} vs remote={remote_etag})"
			else:
				s += f"\x1b[2;34m ({doc.nextcloud_etag})"
		s += '\x1b[m'

		if not _is_real_file:
			s += '\x1b[38;5;110m' + ' -> ' + doc.file_url + '\x1b[m'
		elif not _data_file:
			s += '\x1b[38;5;215;3m' + ' - MISSING LOCAL FILE' + '\x1b[m'

		if doc.nextcloud_exclude:
			s += '\x1b[38;5;160;3m' + ' - nextcloud_exclude=True' + '\x1b[m'

		print(s)


@nextcloud.command('repair')
@pass_context
def repair(ctx, nc_id):
	frappe.connect(site=get_site(ctx))

	filters = {'nextcloud_id': ('!=', '')}
	fields = ['nextcloud_id']
	frappe.get_all('File', fields=fields, filters=filters)
	frappe.db.commit()


@nextcloud.command('connect')
@click.argument('username')
@click.argument('password')
@pass_context
def connect(ctx, username, password):
	frappe.connect(site=get_site(ctx))
	settings = frappe.get_single('Nextcloud Settings')

	try:
		server_is_up = settings.nc_ping_server()
		if server_is_up:
			status = click.style('up', fg='green', bold=True)
		else:
			status = click.style('down', fg='red', bold=True)
		click.echo('Nextcloud server is ' + status)
	except Exception as e:
		click.echo(''.join(traceback.format_exception(0, e, e.__traceback__)), err=True)
		click.echo(click.style(str(e), fg='red'), err=True)
		return

	try:
		settings._get_credentials = lambda: (username, password)
		client = settings.nc_connect()
		click.echo('Nextcloud login is ' + click.style('correct', fg='green', bold=True))

		print('frappe, client')
		import pdb; pdb.set_trace()
	except Exception as e:
		click.echo(''.join(traceback.format_exception(0, e, e.__traceback__)), err=True)
		click.echo(click.style(str(e), fg='red'), err=True)
		click.echo('Nextcloud login is ' + click.style('incorrect', fg='red', bold=True))
		return

commands = [
	nextcloud,
]
