from frappe.custom.doctype.custom_field.custom_field import create_custom_fields

def after_install():
	create_custom_fields(get_custom_fields())

def after_migrate():
	create_custom_fields(get_custom_fields())

def get_custom_fields():
	return {
		"File": [
			{
				"fieldname": "nextcloud_id",
				"fieldtype": "Data",
				"hidden": 1,
				"label": "Nextcloud ID",
				"no_copy": 1,
				"print_hide": 1,
				"report_hide": 1,
				"unique": 1
			},
			{
				"fieldname": "nextcloud_parent_id",
				"fieldtype": "Data",
				"hidden": 1,
				"label": "Nextcloud Parent ID",
				"no_copy": 1,
				"print_hide": 1,
				"report_hide": 1
			},
			{
				"fieldname": "nextcloud_etag",
				"fieldtype": "Data",
				"hidden": 1,
				"label": "Nextcloud ETag",
				"no_copy": 1,
				"print_hide": 1,
				"report_hide": 1
			},
			{
				"fieldname": "nextcloud_exclude",
				"fieldtype": "Check",
				"hidden": 0,
				"insert_after": "content_hash",
				"label": "Exclude from Nextcloud sync",
				"no_copy": 1,
				"print_hide": 1,
				"read_only_depends_on": "eval:doc.nextcloud_id",
				"report_hide": 1
			},
		]
	}
