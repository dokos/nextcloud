from . import __version__ as app_version

app_name = "nextcloud"
app_title = "Nextcloud"
app_publisher = "Dokos SAS"
app_description = "Nextcloud Integration with Dodock"
app_email = "hello@dokos.io"
app_license = "GNU AGPLv3"

# Includes in <head>
# ------------------

# include js, css files in header of desk.html
# app_include_css = "/assets/nextcloud/css/nextcloud.css"
# app_include_js = "/assets/nextcloud/js/nextcloud.js"

# include js, css files in header of web template
# web_include_css = "/assets/nextcloud/css/nextcloud.css"
# web_include_js = "/assets/nextcloud/js/nextcloud.js"

# include custom scss in every website theme (without file extension ".scss")
# website_theme_scss = "nextcloud/public/scss/website"

# include js, css files in header of web form
# webform_include_js = {"doctype": "public/js/doctype.js"}
# webform_include_css = {"doctype": "public/css/doctype.css"}

# include js in page
# page_js = {"page" : "public/js/file.js"}

# include js in doctype views
# doctype_js = {"doctype" : "public/js/doctype.js"}
# doctype_list_js = {"doctype" : "public/js/doctype_list.js"}
# doctype_tree_js = {"doctype" : "public/js/doctype_tree.js"}
# doctype_calendar_js = {"doctype" : "public/js/doctype_calendar.js"}

# Home Pages
# ----------

# application home page (will override Website Settings)
# home_page = "login"

# website user home page (by Role)
# role_home_page = {
#	"Role": "home_page"
# }

# Generators
# ----------

# automatically create page for each record of this doctype
# website_generators = ["Web Page"]

# Jinja
# ----------

# add methods and filters to jinja environment
# jinja = {
# 	"methods": "nextcloud.utils.jinja_methods",
# 	"filters": "nextcloud.utils.jinja_filters"
# }

# Installation
# ------------

# before_install = "nextcloud.install.before_install"
after_install = "nextcloud.install.after_install"
after_migrate = "nextcloud.install.after_migrate"

# Uninstallation
# ------------

# before_uninstall = "nextcloud.uninstall.before_uninstall"
# after_uninstall = "nextcloud.uninstall.after_uninstall"

# Desk Notifications
# ------------------
# See frappe.core.notifications.get_notification_config

# notification_config = "nextcloud.notifications.get_notification_config"

# Permissions
# -----------
# Permissions evaluated in scripted ways

# permission_query_conditions = {
# 	"Event": "frappe.desk.doctype.event.event.get_permission_query_conditions",
# }
#
# has_permission = {
# 	"Event": "frappe.desk.doctype.event.event.has_permission",
# }

# DocType Class
# ---------------
# Override standard doctype classes

# override_doctype_class = {
# 	"ToDo": "custom_app.overrides.CustomToDo"
# }

# Document Events
# ---------------
# Hook on document methods and events

doc_events = {
	"File": {
		"after_insert": [
			"nextcloud.nextcloud.doctype.nextcloud_settings.nextcloud_filesync.api.file_on_create",
		],
		"on_trash": [
			"nextcloud.nextcloud.doctype.nextcloud_settings.nextcloud_filesync.api.file_on_trash",
		],
		"on_update": [
			"nextcloud.nextcloud.doctype.nextcloud_settings.nextcloud_filesync.api.file_on_update",
		],
	}
}

# Scheduled Tasks
# ---------------

scheduler_events = {
	"all": [
		"nextcloud.nextcloud.doctype.nextcloud_settings.nextcloud_filesync.cron.run_cron"
	],
	# "daily": [
	# 	"nextcloud.tasks.daily"
	# ],
	# "hourly": [
	# 	"nextcloud.tasks.hourly"
	# ],
	# "weekly": [
	# 	"nextcloud.tasks.weekly"
	# ],
	# "monthly": [
	# 	"nextcloud.tasks.monthly"
	# ],
}

# Testing
# -------

# before_tests = "nextcloud.install.before_tests"

# Overriding Methods
# ------------------------------
#
# override_whitelisted_methods = {
# 	"frappe.desk.doctype.event.event.get_events": "nextcloud.event.get_events"
# }
#
# each overriding function accepts a `data` argument;
# generated from the base implementation of the doctype dashboard,
# along with any modifications made in other Frappe apps
# override_doctype_dashboards = {
# 	"Task": "nextcloud.task.get_dashboard_data"
# }

# exempt linked doctypes from being automatically cancelled
#
# auto_cancel_exempted_doctypes = ["Auto Repeat"]


# User Data Protection
# --------------------

# user_data_fields = [
# 	{
# 		"doctype": "{doctype_1}",
# 		"filter_by": "{filter_by}",
# 		"redact_fields": ["{field_1}", "{field_2}"],
# 		"partial": 1,
# 	},
# 	{
# 		"doctype": "{doctype_2}",
# 		"filter_by": "{filter_by}",
# 		"partial": 1,
# 	},
# 	{
# 		"doctype": "{doctype_3}",
# 		"strict": False,
# 	},
# 	{
# 		"doctype": "{doctype_4}"
# 	}
# ]

# Authentication and authorization
# --------------------------------

# auth_hooks = [
# 	"nextcloud.auth.validate"
# ]

# Translation
# --------------------------------

# Make link fields search translated document names for these DocTypes
# Recommended only for DocTypes which have limited documents with untranslated names
# For example: Role, Gender, etc.
# translated_search_doctypes = []
