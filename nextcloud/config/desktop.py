from frappe import _

def get_data():
	return [
		{
			"module_name": "Nextcloud",
			"type": "module",
			"label": _("Nextcloud")
		}
	]
